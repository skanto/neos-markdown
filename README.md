
## Install:

`composer require skanto/neos-markdown`

## Use:
MyTemplate.html
```
{namespace markdown=Skanto\NeosMarkdown\ViewHelpers}
{myVariable -> markdown:format.markdown() -> f:format.raw()}
```
